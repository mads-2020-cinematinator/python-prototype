#!/bin/bash
# Run the code formatting  tools on our project.
nix-shell -A package --run 'isort .; black setup.py cinematinator'
