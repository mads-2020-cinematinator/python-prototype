"""
This is the main entrypoint to our project's code.

See the subcommands' --help options for more specific help.
"""
import enum
from typing import Dict, TextIO

import click

from cinematinator.ilp import MIPSolver
from cinematinator.instance_io import (
    DebugLetters,
    RequiredLetters,
    parse_cinema,
    parse_group_counts,
    prettyprint_cinema,
)
from cinematinator.offline import BruteforceOfflineSolver
from cinematinator.online import online_solvers
from cinematinator.simple_cinema import SimpleCinema
from cinematinator.types import OfflineSolverProtocol


@click.group()
def cli() -> None:
    """
    This is the main entrypoint to our project's code.

    See the following subcommands' --help for usage.
    """
    pass


@cli.command("print-instance", short_help="Read and prettyprint a Cinema")
@click.argument("input_file", type=click.File(mode="r"))
def print_instance_cmd(input_file: TextIO) -> None:
    """
    Debug: Read and pretty-print a Cinema instance.

    Pass '-' as the filename to read from standard input (which lets you paste in a
    Cinema).

    Debug command.
    """

    cinema = parse_cinema(input_file)
    print(prettyprint_cinema(cinema, DebugLetters))


class OfflineSolvers(enum.Enum):
    bruteforce = BruteforceOfflineSolver
    mip = MIPSolver


@cli.command("offline")
@click.option(
    "--solver",
    "solver_name",
    type=click.Choice(OfflineSolvers._member_names_),  # type:ignore
    default="mip",
    help="Undocumented debug option",
)
@click.argument("input_file", type=click.File(mode="r"))
def offline_cmd(solver_name: str, input_file: TextIO) -> None:
    """
    Read offline instance, solve using MIP.

    Reads a Cinema instance and group counts from a file, then solves the problem for
    this Cinema and group counts using an offline solver, either a MIP solver.

    Pass '-' as the filename to read from standard input instead of a file.

    For debugging an undocumented bruteforce solver is selectable using `--solver
    bruteforce`, this will not work for most instances.
    """

    solver = OfflineSolvers[solver_name].value

    cinema = parse_cinema(input_file)
    group_counts = parse_group_counts(input_file)

    instance = solver(cinema=cinema, groups=group_counts)
    result = instance.solve()

    print(prettyprint_cinema(result, RequiredLetters))


@cli.command("edge-algo")
@click.argument("input_file", type=click.File(mode="r"))
def edge_algo_cmd(input_file: TextIO) -> None:
    """
    Read offline instance, solve using combinatorial algorithm.

    Reads a Cinema instance from the given file and the group counts in this file, then
    solves this instance with the combinatorial / edge algorithm.

    The only output will be the number of people that can optimally be seated for the
    input.

    Pass '-' as the input filename to read from standard input instead of a file.
    """

    cinema = parse_cinema(input_file)
    group_counts = parse_group_counts(input_file)

    instance = SimpleCinema(cinema=cinema, groups=group_counts)
    result = instance.solve(dict())

    print(result)


@cli.command("online")
@click.argument("input_file", type=click.File(mode="r"))
@click.option(
    "--mode",
    "-m",
    "mode_name",
    type=click.Choice(online_solvers.keys()),
    default="first-fit",
    help="Which seat assignment mode to use.",
)
@click.option(
    "--print-when-done/--no-print-when-done",
    "-p",
    default=False,
    help="Print the final Cinema when input ends?",
)
def online_cmd(input_file: TextIO, mode_name: str, print_when_done: bool) -> None:
    """
    Read online instance, solve using chosen strategy.

    Reads a Cinema instance from the given file and initialized a Cinema object, then
    repeatedly read group sizes from the input and attempt to seat each read group
    before moving on to the next group.

    Pass '-' as the input filename to read from standard input instead of a file.

    Currently the following modes are supported with --mode:

    \b
      - "first-fit":
          Fit groups in the leftmost position in the top row where the group fits.
      - "last-fit":
          Fit groups in the rightmost position in the bottom row where the group fits.
      - "greedy":
          Fit groups in a position where they cause a minimum amount of still-available
          seats to be blocked. Also called "best-fit" in our paper.
      - "center":
          Try to fit groups as closest to the center of the cinema as possible.
          Not described in our paper.
    """

    cinema = parse_cinema(input_file)

    solver = online_solvers[mode_name](cinema)

    for line in input_file:
        input = line.strip()

        group_size = int(input)

        if group_size == 0:
            print(solver.cinema.get_amount_people_seated())

        else:
            maybe_location = solver.try_seat_group(group_size)

            if maybe_location is None:
                print("0 0")

            else:
                x, y = maybe_location
                print(f"{x+1} {y+1}")

    if print_when_done:
        print(prettyprint_cinema(solver.cinema, DebugLetters))
        print(f"Seated people: {cinema.get_amount_people_seated()}")


if __name__ == "__main__":
    cli()
