from __future__ import annotations

from functools import partial
from math import ceil
from typing import Callable, Dict, List, Optional, Tuple

from cinematinator.types import Cinema, Coordinates, OnlineSolverProtocol

GroupSize = int
Heuristic = Callable[[Cinema, Coordinates, GroupSize], int]


online_solvers: Dict[str, Callable[[Cinema], OnlineSolverProtocol]] = {
    "first-fit": lambda c: OnlineFirstFitSolver(c),
    "last-fit": lambda c: OnlineLastFitSolver(c),
    "greedy": lambda c: OnlineHeuristicSolver(c, heuristic=greedy_heuristic),
    "center": lambda c: OnlineHeuristicSolver(c, heuristic=center_heuristic),
}


class OnlineFirstFitSolver:
    def __init__(self, cinema: Cinema) -> None:
        self.cinema = cinema

    def try_seat_group(self, group_size: GroupSize) -> Optional[Coordinates]:
        """
        Try to seat a group using the "first-fit" strategy.

        Iterate over each row from left to right, top to bottom, and seat a group in the
        first found position that will fit.
        """

        for y in range(self.cinema.height):
            for x in range(self.cinema.width):
                if self.cinema.can_seat_group(x, y, group_size):
                    self.cinema.seat_group(x, y, group_size)
                    return (x, y)

        return None


class OnlineLastFitSolver:
    def __init__(self, cinema: Cinema) -> None:
        self.cinema = cinema

    def try_seat_group(self, group_size: GroupSize) -> Optional[Coordinates]:
        """
        Try to seat a group using the "last-fit" strategy.

        Iterate over each row from right to left, bottom to top, and seat a group in the
        first found position that will fit.

        Identical to first-fit except for `reversed` usages.
        """
        for y in reversed(range(self.cinema.height)):
            for x in reversed(range(self.cinema.width)):
                if self.cinema.can_seat_group(x, y, group_size):
                    self.cinema.seat_group(x, y, group_size)
                    return (x, y)

        return None


class OnlineHeuristicSolver:
    """
    Allows choosing positions based on a heuristic function, which is calculated for
    each spot that could fit a group of the given size.

    If two locations return the same minimum value for a group size the top-leftmost
    position is chosen.
    """

    def __init__(self, cinema: Cinema, heuristic: Heuristic) -> None:
        self.cinema = cinema
        self.heuristic = heuristic

    def try_seat_group(self, group_size: GroupSize) -> Optional[Coordinates]:
        """
        Calculate the penalty value for each of the positions that will fit a group of
        the given size, then choose the "best" location (with minimum penalty).

        If no such position exists None is returned.
        """
        potential_locations: List[Tuple[int, Coordinates]] = []

        for y in range(self.cinema.height):
            potential_locations.extend(self.scan_row(group_size, y))

        potential_locations.sort()

        if len(potential_locations) == 0:
            return None

        penalty, optimal_location = potential_locations[0]
        seat_at_x, seat_at_y = optimal_location

        self.cinema.seat_group(seat_at_x, seat_at_y, group_size)

        return optimal_location

    def scan_row(self, group_size: GroupSize, y: int) -> List[Tuple[int, Coordinates]]:
        """
        Scan the row at the given height for open spots that fit the given group size, then
        return the starting location that fits the group while returning the smallest
        penalty as defined by the passed heuristic.

        If there are multiple eligible locations that have the same penalty, return the
        top-leftmost position.
        """

        potential_locations: List[Tuple[int, Coordinates]] = []

        # Optimization: we will never be able to seat a group at a position less than
        # group_size from the right edge.
        for x in range(0, self.cinema.width - group_size + 1):
            if self.cinema.can_seat_group(x, y, group_size):
                penalty = self.heuristic(self.cinema, (x, y), group_size)

                potential_locations.append((penalty, (x, y)))

        return potential_locations


def greedy_heuristic(cinema: Cinema, spot: Coordinates, group_size: GroupSize) -> int:
    """
    "Greedy" heuristic: returns the amount of seats that would be newly blocked by
    placing a group of this size at this spot in this cinema.
    """
    x, y = spot
    return cinema.newly_blocked_seats(x, y, group_size)


def center_heuristic(cinema: Cinema, spot: Coordinates, group_size: GroupSize) -> int:
    """
    "Center" heuristic: prefers seating groups near to the center of the cinema (as
    "these seats have a better view").
    Not intended to be competitive or described in the paper.
    """
    x, y = spot
    left_edge = x
    right_edge = x + group_size

    center = cinema.width / 2
    center_difference = min(abs(left_edge - center), abs(right_edge - center))

    return ceil(center_difference)
