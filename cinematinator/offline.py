import copy
from dataclasses import dataclass
from typing import List

from cinematinator.types import Cinema, GroupCounts, OfflineSolverProtocol


class BruteforceOfflineSolver(OfflineSolverProtocol):
    """
    This class models and solves a problem of the `offline` variation.
    """

    def __init__(self, cinema: Cinema, groups: GroupCounts) -> None:
        self.cinema = cinema
        self.groups = groups

    def solve(self) -> Cinema:
        """
        Very slow, very ugly brute force for now.
        """
        return self._solve(self.cinema, self.groups)

    def _get_group_sizes(self, groups: GroupCounts) -> List[int]:
        """
        Return which group sizes have a non-zero amount of groups remaining.

        Example:
            if groups == [1, 0, 2, 1, 0, 0, 1, 0],
            we return [1, 3, 4, 7]
        """

        group_sizes = []

        for group_size_minus_1, count in enumerate(groups):
            for _ in range(count):
                group_sizes.append(group_size_minus_1 + 1)

        return group_sizes

    def _solve(self, cinema: Cinema, groups: GroupCounts) -> Cinema:
        solution_cinemas: List[Cinema] = []

        for group_size in self._get_group_sizes(groups):
            for x in range(cinema.width):
                for y in range(cinema.height):
                    if cinema.can_seat_group(x, y, group_size):
                        c2 = copy.deepcopy(cinema)
                        c2.seat_group(x, y, group_size)
                        g2 = copy.deepcopy(groups)
                        g2[group_size - 1] -= 1
                        solution_cinemas.append(self._solve(c2, g2))

        if len(solution_cinemas) == 0:
            return cinema

        best_cinema = solution_cinemas[0]
        best_cinema_score = -1

        for solution_cinema in solution_cinemas:
            solution_cinema_score = solution_cinema.get_amount_people_seated()
            if solution_cinema_score >= best_cinema_score:
                best_cinema = solution_cinema
                best_cinema_score = solution_cinema_score

        return best_cinema
