# Make Python ignore type annotations and treat them as strings.
# Default behaviour from Python 3.10, needed to do things like `array[int]`.
from __future__ import annotations

import array
import enum
import itertools
from dataclasses import dataclass, field
from enum import IntEnum
from typing import ByteString, List, Optional, Protocol, Tuple

# Helper type to represent coordinates in the Cinema.
# The typechecker will treat this as equivalent to (int, int).
Coordinates = Tuple[int, int]

# Helper type to represent a 8-tuple of integers, which corresponds with the number of
# groups of each size in the offline version of the problem.
GroupCounts = List[int]


class SeatStatus(IntEnum):
    """
    Internal representation of the "status" of a seat, i.e. whether it's available to
    assign to guests.
    All values of this enum are integers to be able to put them in an `array.array`.
    """

    # There's no seat here
    NoSeat = ord(" ")

    # Seat can be assigned to a group
    Available = ord("o")

    # Seat has been taken
    Taken = ord("i")

    # Seat can't be used because it's within block range of one or more other groups.
    Blocked = ord("x")


@dataclass
class Cinema:
    width: int
    height: int
    seats: array.array[int] = field(init=False)

    def __post_init__(self) -> None:
        if self.width <= 0:
            raise ValueError(f"Width must be a positive integer, is {self.width}")
        if self.height <= 0:
            raise ValueError(f"Height must be a positive integer, is {self.height}")

        self.seats = array.array(
            "b", itertools.repeat(SeatStatus.NoSeat.value, self.width * self.height)
        )

    def _xy_to_index(self, x: int, y: int) -> int:
        if not 0 <= x < self.width:
            raise IndexError(f"`x` out of range: got {x}, width {self.width}")
        if not 0 <= y < self.height:
            raise IndexError(f"`y` out of range: got {y}, width {self.height}")

        return x + self.width * y

    def __getitem__(self, key: Coordinates) -> SeatStatus:
        """
        Magic method to allow index access on a Cinema.
        This allows doing `cinema[x, y]` to get the status for some seat.

        Docs: https://docs.python.org/3/reference/datamodel.html#object.__getitem__
        """

        x, y = key
        return self.get_seat(x, y)

    def __setitem__(self, key: Coordinates, value: SeatStatus) -> None:
        """
        Magic method to allow index setting on a Cinema.
        This allows doing `cinema[x, y] = SeatStatus.[...]` to set some seat.

        Docs: https://docs.python.org/3/reference/datamodel.html#object.__setitem__
        """

        x, y = key
        self.set_seat(x, y, value)

    def get_seat(self, x: int, y: int) -> SeatStatus:
        value = self.seats[self._xy_to_index(x, y)]
        return SeatStatus(value)

    def set_seat(self, x: int, y: int, new_status: SeatStatus) -> None:
        self.seats[self._xy_to_index(x, y)] = new_status.value

    def can_seat_group(self, x: int, y: int, group_size: int) -> bool:
        """
        Returns wether you can seat a group of this size to the
        right starting at the coordinates (x, y).
        """
        try:
            for check_x in range(x, x + group_size):
                if self.get_seat(check_x, y) != SeatStatus.Available:
                    return False
            return True

        # We hit this if we run out of bounds
        except IndexError:
            return False

    def seat_group(self, x: int, y: int, group_size: int) -> None:
        """
        Seats a group in the cinema to the right from the given
        coordinates. Throws an AssertionError if the group can't be
        seated here.
        """
        if not self.can_seat_group(x, y, group_size):
            raise AssertionError("Can't seat a group here")

        # Block seats (including the seats that we will use, for
        # convenience)
        for block_y in range(y - 1, y + 2):
            start_x = x - 1
            end_x = x + group_size + 1

            if block_y == y:
                start_x -= 1
                end_x += 1

            for block_x in range(start_x, end_x):
                try:
                    if self[block_x, block_y] == SeatStatus.Available:
                        self[block_x, block_y] = SeatStatus.Blocked

                except IndexError:
                    # Ignore if we try to block seats out of bounds
                    pass

        # Give the people their seats!
        # No try-catch here since we can't seat people out of bounds
        for seat_x in range(x, x + group_size):
            self.set_seat(seat_x, y, SeatStatus.Taken)

    def get_amount_people_seated(self) -> int:
        """
        Returns the amount of people seated in the cinema
        """
        amount = 0
        for x in range(self.width):
            for y in range(self.height):
                if self.get_seat(x, y) == SeatStatus.Taken:
                    amount += 1

        return amount

    def newly_blocked_seats(self, x: int, y: int, group_size: int) -> int:
        """
        Return the amount of currently available seats that would become blocked if a
        group were to be seated at this location.
        """

        result: int = 0

        # For the row above and the row below the row where people are seated we check
        # "one wider" than where people are seated:
        #
        # xxxxxx
        #  iiii
        # xxxxxx
        min_x = max(x - 1, 0)
        max_x = min(self.width, x + group_size)

        for check_y in (y - 1, y + 1):
            # Skip if this row doesn't exist
            if check_y not in range(0, self.height):
                continue

            for check_x in range(min_x, max_x):
                if self[check_x, check_y] == SeatStatus.Available:
                    result += 1

        # For the same row where we seat people we check two to the left and two to the
        # right of where we seat people:
        #
        # xxiiiixx
        for check_x in (x - 2, x - 1, x + group_size + 1, x + group_size + 2):
            # Skip if this seat doesn't exist (near the edge of the cinema)
            if check_x not in range(0, self.width):
                continue

            if self[check_x, y] == SeatStatus.Available:
                result += 1

        return result


class OfflineSolverProtocol(Protocol):
    """
    Protocol for offline solvers.

    Offline solvers are given an initial Cinema and the counts for each group size as
    input, the output should then be the Cinema object representing the/an optimal
    solution.
    """

    def __init__(self, cinema: Cinema, groups: GroupCounts) -> None:
        ...

    def solve(self) -> Cinema:
        ...


class OnlineSolverProtocol(Protocol):
    """
    Protocol for online solvers.

    Online solvers are given an initial Cinema object as input and then receive one
    group size per iteration, for which the solver returns either the coordinates where
    this groups has been seated or None if the group cannot be seated.
    """

    cinema: Cinema

    def try_seat_group(self, group_size: int) -> Optional[Coordinates]:
        ...
