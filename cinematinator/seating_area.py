from __future__ import annotations

import array
import copy
from dataclasses import dataclass
from functools import cached_property
from typing import List, Tuple


# Seating area used in SimpleCinema
@dataclass()
class SeatingArea:
    width: int
    height: int
    seats: array.array[int]

    def __init__(self, width: int, height: int, seats: array.array[int]) -> None:
        self.width = width
        self.height = height
        self.seats = self.calculate_seating_numbers(seats, 0, (width - 1, height - 1))

    # helper function
    def _xy_to_index(self, x: int, y: int) -> int:
        if not 0 <= x < self.width:
            raise KeyError(f"`x` out of range: got {x}, width {self.width}")
        if not 0 <= y < self.height:
            raise KeyError(f"`y` out of range: got {y}, width {self.height}")

        return x + self.width * y

    # return max group size of the seat (between 0 and 8)
    def get_seat(self, x: int, y: int) -> int:
        return self.seats[self._xy_to_index(x, y)]

    # check if a group of a certain size can be seated on this seat
    def can_seat_group(self, x: int, y: int, group_size: int) -> bool:
        try:
            return self.get_seat(x, y) >= group_size
        except KeyError:
            return False

    # return seating area with group seated
    def seat_group(self, x: int, y: int, group_size: int) -> array.array[int]:
        """
        Seats a group in the seating area to the right from the given
        coordinates. Throws an AssertionError if the group can't be
        seated here.
        Returns the new seating area.
        """
        if not self.can_seat_group(x, y, group_size):
            raise AssertionError("Can't seat a group here")

        new_seating = copy.copy(self.seats)

        # block seats
        for block_y in range(y - 1, y + 2):
            start_x = x - 1
            end_x = x + group_size + 1

            if block_y == y:
                start_x -= 1
                end_x += 1

            for block_x in range(start_x, end_x):
                try:
                    new_seating[self._xy_to_index(block_x, block_y)] = 0
                except KeyError:
                    pass

        # recalculate seating numbers to the left of the group
        return self.calculate_seating_numbers(new_seating, y - 1, (x, y + 1))

    # preprocessing so we can we check which group size can be seated on every seat
    def calculate_seating_numbers(
        self, seats: array.array[int], rangeStartY: int, rangeEnd: Tuple[int, int]
    ) -> array.array[int]:

        for y in range(rangeStartY, rangeEnd[1] + 1):
            current = 0
            for x in range(rangeEnd[0], -1, -1):
                try:
                    if seats[self._xy_to_index(x, y)] == 0:
                        current = 0
                    else:
                        current = min(current + 1, 8)
                        seats[self._xy_to_index(x, y)] = current
                except KeyError:
                    pass

        return seats

    # leftmost edge of the seating area
    def extremals(self) -> List[Tuple[int, int]]:
        last_blocked = True

        extremals: List[Tuple[int, int]] = []

        for y in range(self.height):
            last_blocked = True
            for x in range(self.width):
                if self.get_seat(x, y) > 0 and last_blocked:
                    extremals.append((x, y))
                    last_blocked = False
                if self.get_seat(x, y) == 0:
                    last_blocked = True

        return extremals

    # symmetrical id for caching
    @cached_property
    def id(self) -> int:
        return min(
            self.id_north_east,
            self.id_north_west,
            self.id_south_east,
            self.id_south_west,
        )

    @cached_property
    def id_north_east(self) -> int:
        id = 0

        for i in self.seats:
            if i > 0:
                id = id + 1
            id << 1

        return id

    @cached_property
    def id_south_west(self) -> int:
        id = 0

        for i in reversed(self.seats):
            if i > 0:
                id = id + 1
            id << 1

        return id

    @cached_property
    def id_north_west(self) -> int:
        id = 0

        for y in range(self.height):
            for x in range(self.width - 1, -1, -1):
                i = self.get_seat(x, y)
                if i > 0:
                    id = id + 1
                id << 1

        return id

    @cached_property
    def id_south_east(self) -> int:
        id = 0

        for y in range(self.height - 1, -1, -1):
            for x in range(self.width):
                i = self.get_seat(x, y)
                if i > 0:
                    id = id + 1
                id << 1

        return id

    # helper function
    def print(self) -> str:
        rows: List[str] = []
        for y in range(self.height):
            row: List[str] = []
            for x in range(self.width):
                if self.get_seat(x, y) > 0:
                    row.append(str(self.get_seat(x, y)))
                else:
                    row.append(" ")

            rows.append("".join(row))

        return "\n".join(rows)
