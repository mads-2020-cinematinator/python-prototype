import copy
import time
from itertools import combinations
from typing import List

from mip import BINARY, Model, maximize, minimize, xsum

from cinematinator.instance_io import RequiredLetters, prettyprint_cinema
from cinematinator.types import Cinema, GroupCounts, OfflineSolverProtocol


class MIPSolver(OfflineSolverProtocol):
    """
    This class models and solves an offline variation using ILP
    """

    def __init__(self, cinema: Cinema, groups: GroupCounts) -> None:
        self.cinema = cinema
        self.groups = groups

    def solve(self) -> Cinema:
        return self._solve(self.cinema, self.groups)

    """
    Check if two given groups are in conflict
    """
    # Function that checks if two groups on a certain location are in conflict.
    def in_conflict(self, x1, y1, s1, x2, y2, s2):
        # for each person in group 1
        for sx in range(x1, x1 + s1):
            if abs(y1 - y2) > 1:
                continue
            elif abs(y1 - y2) == 1:
                if sx in range(x2 - 1, x2 + s2 + 1):
                    return True
            else:
                if sx in range(x2 - 2, x2 + s2 + 2):
                    return True
        return False

    # creates a quick feasible solution using firstfit.
    def quickFeasible(self, cinema_temp, group_descr):
        cinema = copy.deepcopy(cinema_temp)
        selected = []
        for group_index in reversed(range(8)):
            seated = 0
            group = group_index + 1
            for y in range(cinema.height):
                for x in range(cinema.width):
                    if (
                        cinema.can_seat_group(x, y, group)
                        and seated < group_descr[group_index]
                    ):
                        cinema.seat_group(x, y, group)
                        seated += 1
                        selected.append((group_index, x, y))
        print(prettyprint_cinema(cinema, RequiredLetters))
        return selected

    # Main solver method for the ILP
    def _solve(self, cinema: Cinema, groups: GroupCounts) -> Cinema:
        # Test input from the pdf
        start_time = time.process_time()
        # create a mip model
        m = Model("cinema")
        nrConstraints = 0

        # create a variable for every group at every feasible position
        print("Creating possible variables...")
        I = []
        for group_index in range(8):
            group_size = group_index + 1
            for x in range(cinema.width):
                for y in range(cinema.height):
                    if cinema.can_seat_group(x, y, group_size):
                        I.append((group_index, x, y, m.add_var(var_type=BINARY)))
        measure1 = time.process_time() - start_time
        print(f"Done in {round(measure1, 2)}s!")
        print("-----------------------")

        print("Creating constraints...")
        for (
            (group_index1, x1, y1, var1),
            (group_index2, x2, y2, var2),
        ) in combinations(I, r=2):
            # if two groups conflict, only 1 can be used
            if self.in_conflict(x1, y1, group_index1 + 1, x2, y2, group_index2 + 1):
                m += var1 + var2 <= 1
                nrConstraints += 1

        # Groups can only be seated a maximum number of times
        for group_index in range(8):
            m += (
                xsum(var for (i, _, _, var) in I if i == group_index)
                <= groups[group_index]
            )
            nrConstraints += 1
        measure2 = time.process_time() - measure1
        print(f"Done in {round(measure2, 2)}s!")
        print("-----------------------")

        print(f"nr of vars {len(I)}, nr of constraints {nrConstraints}")
        print("-----------------------")

        # Define the objective function as the maximisation of total people
        m.objective = maximize(
            xsum(var * (group_index + 1) for (group_index, _, _, var) in I)
        )

        # Calculate an initial feasible solution to help the branch and bound algorithm
        print("Calculating feasible solution as starting point...")
        feasible_selected = self.quickFeasible(cinema, groups)
        print(f"Feasible amount: {sum([i+1 for (i,_,_) in feasible_selected])}")
        measure3 = time.process_time() - measure2
        print(f"Done in {round(measure3, 2)}s")
        m.start = [(var, 1.0) for (i, x, y, var) in I if (i, x, y) in feasible_selected]

        # Solve the mip and print the status
        print(m.optimize(max_seconds=150))

        # Select all the groups that have been selected
        selected = [(group_id, x, y) for (group_id, x, y, var) in I if var.x >= 0.99]

        # Calculate how many people are in those groups
        total_people = sum([group_index + 1 for (group_index, _, _) in selected])

        # Seat the groups in the cinema
        for (group_index, x, y) in selected:
            cinema.seat_group(x, y, group_index + 1)

        # Print the results
        print("Selected items: {}".format(selected))
        print(f"Total people seated: {total_people}")

        seated_vector = [0] * 8
        for (group_id, _, _) in selected:
            seated_vector[group_id] += 1

        return cinema
