from __future__ import annotations

import array
import copy
from typing import Any, Dict, List, Tuple

from cinematinator.seating_area import SeatingArea
from cinematinator.types import Cinema, SeatStatus


# Cinema with seperate seating areas and edge seating theorem implemented
class SimpleCinema:
    areas: List[SeatingArea] = []
    groups: List[int]
    mask: List[Tuple[int, int]]

    def __init__(self, cinema: Cinema, groups: List[int]):
        self.mask = [
            (-1, -1),
            (0, -1),
            (1, -1),
            (-2, 0),
            (-1, 0),
            (1, 0),
            (2, 0),
            (-1, 1),
            (0, 1),
            (1, 1),
        ]
        self.groups = groups
        self.build_areas(cinema)

    # score if all groups can be seated
    def max_score(self) -> int:
        max_score = 0

        for (index, size) in enumerate(self.groups):
            max_score = max_score + (index + 1) * size

        return max_score

    # returns a list of cinema's with one group places, one cinema for each
    # combination of edge seats and groups
    def sub_cinemas(self) -> List[SimpleCinema]:
        subs: List[SimpleCinema]

        for (area_index, area) in enumerate(self.areas):
            for extremal in area.extremals(self.groups):
                for size, amount in enumerate(self.groups):
                    if amount != 0:
                        subs.append(self.seat_group(area_index, extremal, size))

        return subs

    # get seats from Cinema object and run flood_fill() on the seats
    def build_areas(self, cinema: Cinema) -> None:
        seats = array.array("i")
        for y in range(cinema.height):
            for x in range(cinema.width):
                seats.append(1 if cinema.get_seat(x, y) == SeatStatus.Available else 0)

        self.areas = self.flood_fill(cinema.height, cinema.width, seats)

    # Convert list of seats to list of independent seating areas
    # https://en.wikipedia.org/wiki/Connected-component_labeling
    def flood_fill(
        self, height: int, width: int, seats: array.array[int]
    ) -> List[SeatingArea]:
        areas = []

        for y in range(height):  # TODO Change this to single loop maybe.
            for x in range(width):
                if (  # Only do floodfill if position not already encountered and actual seat
                    seats[y * width + x] == 1
                ):
                    # Mark position as seen
                    seats[y * width + x] = -1

                    area = [(x, y)]
                    queue = [(x, y)]

                    # area size limits
                    minX = x
                    minY = y
                    maxX = x
                    maxY = y

                    while queue != []:
                        currentSeat = queue.pop()

                        for xy in self.mask:
                            new = self.combineTuple(currentSeat, xy)
                            if (
                                new[0] >= 0
                                and new[0] < width
                                and new[1] >= 0
                                and new[1] < height
                            ):
                                # Only proceed if not marked and spot contains a seat
                                if seats[new[1] * width + new[0]] >= 1:
                                    # Mark as seen
                                    seats[new[1] * width + new[0]] = -1

                                    if new[0] < minX:
                                        minX = new[0]
                                    if new[0] > maxX:
                                        maxX = new[0]
                                    if new[1] < minY:
                                        minY = new[1]
                                    if new[1] > maxY:
                                        maxY = new[1]

                                    queue.append(new)
                                    area.append(new)

                    areas.append(self.buildArea(area, minX, maxX, minY, maxY))

        return areas

    # helper function
    def combineTuple(self, a: Tuple[int, int], b: Tuple[int, int]) -> Tuple[int, int]:
        return (a[0] + b[0], a[1] + b[1])

    # Create SeatingArea object from list of seats
    def buildArea(
        self,
        positions: List[Tuple[int, int]],
        startX: int,
        endX: int,
        startY: int,
        endY: int,
    ) -> SeatingArea:
        width = endX - startX + 1
        height = endY - startY + 1
        seats = array.array("i", [0] * width * height)
        for (x, y) in positions:
            seats[width * (y - startY) + (x - startX)] = 1

        return SeatingArea(width, height, seats)

    # Return cinema with a single group seated
    def seat_group(
        self, area_index: int, position: Tuple[int, int], size: int
    ) -> SimpleCinema:
        area = self.areas[area_index]
        cinema = copy.deepcopy(self)
        seats = area.seat_group(position[0], position[1], size)
        split_areas = self.flood_fill(area.height, area.width, seats)
        cinema.areas.pop(area_index)
        cinema.areas = cinema.areas + split_areas
        cinema.groups[size - 1] = cinema.groups[size - 1] - 1
        return cinema

    # helper function
    def print(self) -> str:
        lines: List[str] = []

        for area in self.areas:
            lines.append(area.print())
            lines.append("\n")

        lines.append("groups: " + str(self.groups))
        lines.append("max score: " + str(self.max_score()))

        return "\n".join(lines)

    # calculate the maximum score of this cinema
    def solve(self, cache: Dict[SimpleCinema, int]) -> int:
        if len(self.areas) == 0:
            return 0
        scores = []

        if self in cache:
            return cache[self]

        for (area_index, area) in enumerate(self.areas):
            for (index, amount) in enumerate(self.groups):
                size = index + 1
                if amount > 0:
                    for extremal in area.extremals():
                        if area.can_seat_group(extremal[0], extremal[1], size):
                            scores.append(
                                self.seat_group(area_index, extremal, size).solve(cache)
                                + size
                            )
                            if max(scores) == self.max_score():
                                return max(scores)

        score = max(scores) if len(scores) > 0 else 0

        cache[self] = score

        return score

    # hash for caching
    def __hash__(self) -> int:
        return hash(tuple(self.area_ids()) + (0,) + tuple(self.groups))

    # helper function
    def area_ids(self) -> List[int]:
        return sorted(map(lambda x: x.id, (self.areas)))

    # needed for caching
    def __eq__(self, other: object) -> bool:
        if not isinstance(other, SimpleCinema):
            return False

        if self.area_ids() == other.area_ids():
            if tuple(self.groups) == tuple(other.groups):
                return True

        return False
