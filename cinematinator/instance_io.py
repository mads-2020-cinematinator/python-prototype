"""
Provides methods to read Cinema instances and to write them back out again.
"""

import enum
from typing import List, Literal, Mapping, TextIO, Union

from .types import Cinema, GroupCounts, SeatStatus


def parse_cinema(stream: TextIO) -> Cinema:
    """
    Read the first 2 + width lines of the given stream and try to construct a Cinema
    object with the seating arrangement given in the input.

    Returns a Cinema object if successful.

    Raises a ValueError if
      - The width is not a positive integer
      - The height is not a positive integer

    Raises an AssertionError if
      - One of the lines of the rows is not exactly `width` characters long
      - One of the lines with seats contains something other than a '0' or '1'

    The passed `stream` is modified in-place as a side-effect of reading the input.
    """

    # The specification says that height comes first, then width
    height = int(stream.readline().strip())
    width = int(stream.readline().strip())

    # Creates a Cinema without seats so we only need to fill in the seats that exist
    cinema = Cinema(width, height)

    for y in range(height):
        row_str = stream.readline().strip()

        if len(row_str) != width:
            raise AssertionError(
                "Encountered seat row with wrong width: "
                f"expected {width} seats, got '{row_str}'"
            )

        for x, seat_char in enumerate(row_str):
            # Don't need to set nonexistent seats, this is the default
            if seat_char == "0":
                continue

            if seat_char == "1":
                cinema.set_seat(x, y, SeatStatus.Available)

            else:
                raise AssertionError(f"Expected '0' or '1', got '{seat_char}'")

    return cinema


RequiredLetters: Mapping[SeatStatus, str] = {
    SeatStatus.NoSeat: "0",
    SeatStatus.Available: "1",
    SeatStatus.Blocked: "1",
    SeatStatus.Taken: "x",
}

DebugLetters: Mapping[SeatStatus, str] = {
    SeatStatus.NoSeat: " ",
    SeatStatus.Available: "o",
    SeatStatus.Blocked: "x",
    SeatStatus.Taken: "i",
}


CharacterSet = Mapping[SeatStatus, str]


def prettyprint_cinema(cinema: Cinema, characterset: CharacterSet) -> str:
    """
    Pretty-print the given Cinema using either the characters required for the output of
    the offline problem or a more expansive debugging output characterset.
    """

    rows: List[str] = []
    for y in range(cinema.height):
        row: List[str] = []
        for x in range(cinema.width):
            seat_status = cinema.get_seat(x, y)
            row.append(characterset[seat_status])

        rows.append("".join(row))

    return "\n".join(rows)


def parse_group_counts(stream: TextIO) -> GroupCounts:
    """
    Parse a line from the given `stream` as a list of eight group counts.
    """

    line = stream.readline().strip()

    values = line.split(" ")
    assert len(values) == 8, "Did not get exactly 8 values for group counts"
    assert all([x.isdigit() for x in values])

    return [int(x) for x in values]
