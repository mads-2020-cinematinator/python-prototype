# This file defines how to build our project's code as a Python package.
# This requires some arguments:
{
  # The version of Python to use,
  python,
  # A helper script to exclude files ignored in Git from our build,
  nix-gitignore,
  # And the dependencies needed to build and check our code.
  runDependencies,
  testDependencies,
  nativeBuildInputs ? [],
}:

python.pkgs.buildPythonPackage {
  name = "cinematinator";
  # Version is required, we set it to zero.
  version = "0";

  # The project's code is contained in the current directory, but any files
  # that are not relevant to the build can be excluded.
  # These files are either listed in the .gitignore (which prevents the files
  # from being tracked in Git) or listed here (for e.g. documentation that is
  # tracked in Git).
  src = nix-gitignore.gitignoreSource [
    # These directories can be excluded:
    "benchmark"
    "instances"
    "nix"
    "testcases"

    # .nix-files are not needed in the build process itself.
    "*.nix"
    # .sh-files are used by developers and currently not run during the build.
    "*.sh"
    # .md-files are documentation
    "*.md"
  ] ./.;

  # Make the passed list of dependencies available to our project's build
  # environment and check environment.
  propagatedBuildInputs = runDependencies python.pkgs;
  nativeBuildInputs = nativeBuildInputs;
  checkInputs = testDependencies python.pkgs;

  checkPhase = ''
    black --check .
    isort --check-only .
    # Skip, broken
    # mypy --strict cinematinator
  '';
}
