"""
This file defines to `setuptools`, the default Python packaging tool, how to package our
project and to register an entry point `cli` that runs our code.

See: https://setuptools.readthedocs.io/en/latest/
"""
from setuptools import setup  # type:ignore[import]

setup(
    name="cinematinator",
    packages=["cinematinator"],
    description="Stamp die bioscoop maar vol",
    install_requires=["click", "mip"],
    include_package_data=True,
    entry_points={"console_scripts": ["cli=cinematinator.cli:cli"]},
    zip_safe=False,
)
