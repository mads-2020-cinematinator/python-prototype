# Cinematinator: Python prototype

This repository contains the code for the paper "The Social Distance Cinema
Seating Problem", a paper for the Algorithms for Decision Support course at
Utrecht University.

The authors of this paper and repository are:

  - Hendrik Gijsbert van Hoef
  - Niels Kwadijk
  - Hugo Peters
  - Pim de Vroom
  - Marien Raat
  - Maarten van den Berg

## Installation on a local machine
Our project is intended to be executed in two contexts: 
  - a local development environment, which requires administrative rights on
    the machine used to run the code, or
  - the benchmarking environment on the Gemini server,  a server provided by
    the UU's Department of Science that we have used to perform our
    experiments.

This README describes the steps needed to set up our project to run in the
development environment, please see the `benchmark` directory for instructions
on how to set up the benchmarking environment.

### Nix
In order to ensure a reproducible software environment we use [Nix], the fully-functional package
manager. Nix needs to be installed on a machine before it can be used. Nix is available for use on
Unix-like operating systems, either macOS or Linux should work.

To install Nix on Linux, execute the following command in a terminal and follow the given instructions:

```console
$ sh <(curl -L https://nixos.org/nix/install)
```

To install Nix on macOS, execute the following command in a terminal and follow the given instructions:

```console
$ sh <(curl -L https://nixos.org/nix/install) --darwin-use-unencrypted-nix-store-volume
```

Afterwards, reopen your terminal or execute the following command to make the `nix` command available.

```console
$ source ~/.nix-profile/etc/profile.d/nix.sh
$ # This should now work:
$ nix

Usage: nix <COMMAND> <FLAGS>... <ARGS>...

[...]
```

For more information on installing Nix please see [Chapter 4 of the Nix manual][nix-install].

[nix-install]: https://nixos.org/manual/nix/stable/#ch-installing-binary

### Dependencies and running

This project's dependencies are managed using [Nix].
The Nix package manager ensures that any user of our code is using the exact development environment
we used to write our code, i.e. the same Python version and closure of dependencies is used.

To install all the project's dependencies and run our code, execute `nix run -c cli --help`.
`cli` is the main entrypoint of our codebase and contains further help text on how to run our
algorithm for the online and offline variants of the cinema seating problem.

There exist a number of additional helper scripts in this repository which wrap the `nix` command.
In short:
  - `./make-virtualenv.sh` creates a "virtualenv" under the `venv` directory, which can be used to
    configure the Python version used in an IDE or editor. This virtualenv contains the exact same
    version of all libraries used to run the project's code.
  - `./develop.sh` opens a new shell which has the PATH variable altered such that this virtualenv's
    programs are available to execute (i.e. `python` refers to the version of Python used in our
    project).
  - `./format.sh` applies our code formatting rules to the project's code.
    If these rules are not applied the 
  - `./typecheck.sh` runs the [Mypy] typechecker on our project's code.
    The typechecker is unable to check the types of some libraries and may give spurious warnings,
    it is not required for the typechecker to pass to execute our project's code.

[Mypy]: https://mypy-lang.org

Useful commands:

- `nix build -o venv -A pythonEnv` (shortcut: `./make-virtualenv.sh`):
  Creates a symlink named `venv` in the root of the repository with the
  project's dependencies inside.
  Of interest:
    - `result/bin/python`, `result/bin/python3.8`:
      Python interpreter with all dependencies available
    - `result/bin/black`, `result/bin/mypy`, `result/bin/isort`:
      Linter tools also used in the build

- `nix-shell -A package` (shortcut: `./develop.sh`):
  Open a new shell with all dependencies active, don't run any code checks.

- `cli` (run this inside the virtualenv):
  Run some command in the project, see `cinematinator/cli.py`.

- Code style commands (run these inside the virtualenv):
  - `isort --recursive cinematinator`:
    Sort imports in all files in the project.
  - `black cinematinator`:
    Apply the [Black] codestyle to all Python files in the project.

  These commands can be run with `./format.sh`.

- `mypy --strict cinematinator` (shortcut: `./typecheck.sh`):
  Run type checks, i.e. check that all functions are called with valid
  arguments, variables aren't misused, etc.

[Nix]: https://nixos.org/nix/
[Black]: https://black.readthedocs.io
