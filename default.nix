# This file defines the build environment used by the `nix` command.
let
  # We use the `niv` tool to "pin" a specific set of packages to be made
  # available in our build environment.
  # See: https://github.com/nmattia/niv

  sources = import ./nix/sources.nix {};
  pkgs = import sources.nixpkgs {};

  # These Python packages are made available to our project at runtime.
  runDependencies = ps: [
    # Command-line argument parsing
    ps.click
    # Interface with CBC / Gurobi ILP solver
    ps.mip
    # Python build/execution tooling
    ps.setuptools
  ];

  # These Python packages are made available to our project at build time only,
  # and are not needed to execute our code.
  testDependencies = ps: [
    # Code formatting
    ps.black
    ps.isort

    # Typechecker
    ps.mypy
  ];

  # In order to run our code we need to define specific version of some
  # packages which are not in the Nix package set by default.
  # We define the new Python environment to be based on Python 3.8:
  python = pkgs.python38.override {
    # ... and to contain new or overridden packages:
    packageOverrides = self: super: {
      # python-mip, which is needed to interface with CBC / Gurobi, is not
      # present by default.
      mip = self.buildPythonPackage rec {
        pname = "mip";
        version = "1.12.0";

        src = self.fetchPypi {
          inherit pname version;
          sha256 = "5fbe42f6532efe6c2431955c12bd4661096e3721e6185722342c3e39d71e62f7";
        };

        propagatedBuildInputs = [self.cffi];

        nativeBuildInputs = [ pkgs.autoPatchelfHook ];

        buildInputs = [ pkgs.zlib self.cppy ];

        doCheck = false;
      };

      # isort, a code quality tool, is at an older version in the pinned
      # version of the Nix package set.
      isort = self.buildPythonPackage rec {
        pname = "isort";
        version = "5.5.4";

        src = self.fetchPypi {
          inherit pname version;
          sha256 = "ba040c24d20aa302f78f4747df549573ae1eaf8e1084269199154da9c483f07f";
        };

        propagatedBuildInputs = [self.colorama pkgs.git];

        doCheck = false;
      };
    };
  };

  # With our overriden Python version we can then define the Python environment
  # we want to use; this consists of the chosen Python interpreter configured
  # with the set of runtime and build-time dependencies defined above.
  # This environment is used for development.
  pythonEnv = python.withPackages (ps: runDependencies ps ++ testDependencies ps);

  # With this Python version we can also define how to build our project's code.
  # This is defined in a separate file, which is `import`ed here:
  package = pkgs.callPackage ./cinematinator.nix {
    inherit python runDependencies testDependencies;
  };

in
  { inherit pythonEnv package; }
