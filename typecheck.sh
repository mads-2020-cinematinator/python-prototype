#!/bin/bash
# Run the Mypy typechecker on our project's code.
nix-shell -A package --run 'mypy --strict cinematinator'
