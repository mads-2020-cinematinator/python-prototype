#!/bin/bash

# This script will run a online benchmark testcase and will log the output and memory/CPU statistics.
# Use with `qsub -M 'YOUR_EMAIL@students.uu.nl' run-online-benchmark.sh BENCHMARK_NUMBER MODE`.

#$ -S /bin/bash
#$ -cwd
#$ -N run-online-benchmark
#$ -m bes

# Limit runtime to five minutes
#$ -l h_rt=0:05:00

set -euxo pipefail

scratch_dir=/scratch/${USER}
export PROOT_NO_SECCOMP=1

benchmark_number=$1
benchmark_mode=$2

$scratch_dir/proot -b ${scratch_dir}/nix-store:/nix bash <<EOF
  set -euxo pipefail
  . ~/.nix-profile/etc/profile.d/nix.sh

  cd ${scratch_dir}/python-prototype
  nix run -f default.nix package -c cli online --mode ${benchmark_mode} --print-when-done ./testcases/Online${benchmark_number}.txt
EOF
