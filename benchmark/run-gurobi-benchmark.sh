#!/bin/bash

# This script will run a offline benchmark testcase using Gurobi and will log the output and memory/CPU statistics.
# Use with `qsub -M 'YOUR_EMAIL@students.uu.nl' run-offline-benchmark.sh BENCHMARK_NUMBER MODE`.

#$ -S /bin/bash
#$ -cwd
#$ -N run-gurobi-benchmark
#$ -q all.q
#$ -m bes

# Limit runtime to ten minutes
#$ -l h_rt=0:10:00

set -euxo pipefail

scratch_dir=/scratch/${USER}
export PROOT_NO_SECCOMP=1

benchmark_number=$1

# Additional workaround: Gurobi does not like the system used on the Gemini server to translate
# Solis users to Linux users, so we pretend to be root here as the license check otherwise fails.
# This is achieved using the `-0` flag.
$scratch_dir/proot -b ${scratch_dir}/nix-store:/nix -0 bash <<EOF
  set -euxo pipefail
  . ~/.nix-profile/etc/profile.d/nix.sh

  cd ${scratch_dir}/python-prototype
  export GUROBI_HOME=${scratch_dir}/gurobi903/linux64
  export GRB_LICENSE_FILE=${scratch_dir}/gurobi.lic
  export PATH=\${PATH}:\${GUROBI_HOME}/bin
  export SOLVER_NAME=GUROBI
  nix run -f default.nix package -c cli offline ./testcases/Exact${benchmark_number}.txt
EOF
