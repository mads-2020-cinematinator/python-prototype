#!/bin/bash

# This script will use the Nix environment bootstrapped by bootstrap-nix-environment.sh to clone the
# Cinematinator project to the scratch directory and set up all required dependencies.

# This script can be submitted via SGE with: qsub -M "YOUR_EMAIL@students.uu.nl" bootstrap-project-environment.sh

#$ -S /bin/bash
#$ -cwd
#$ -N bootstrap-project-environment
#$ -m bes

# Enable "Bash strict mode": exit on error, undefined variables, also log executed commands
set -euxo pipefail

# Set up environment
scratch_dir=/scratch/${USER}
export PROOT_NO_SECCOMP=1

# Enter Nix environment
$scratch_dir/proot -b ${scratch_dir}/nix-store:/nix bash <<EOF
  set -euxo pipefail
  . ~/.nix-profile/etc/profile.d/nix.sh

  # Navigate to temporary directory and download the project's code via Git
  cd ${scratch_dir}
  git clone https://git.science.uu.nl/mads-2020-cinematinator/python-prototype.git --depth 1

  # Open the project and download all dependencies, then build the project's code.
  cd python-prototype
  nix-build -A package
EOF
