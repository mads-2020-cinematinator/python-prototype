#!/bin/bash

# This script will bootstrap a Nix environment under /scratch when executed directly or when submitted via the SGE system.
# Execute by calling the script or submit this script via `qsub -M "your_email@students.uu.nl" bootstrap-nix-environment.sh`.

# Magic SGE options:

# Job name
#$ -N bootstrap-nix-environment

# Execute via Bash in the current directory
#$ -S /bin/bash
#$ -cwd

# Send mail on job start, end and suspension (requires passing -M when submitting)
#$ -m bes

# Bash "strict mode": terminate if a command fails or an undefined variable is used.
# Also enable verbose debug logging of which commands are run.
set -euxo pipefail

# Make scratch directory to play around in.
scratch_dir=/scratch/${USER}

mkdir -p ${scratch_dir}/nix-store
cd $scratch_dir

# Download PRoot, verify and mark file executable
proot_url="https://gitlab.com/proot/proot/-/jobs/710225783/artifacts/raw/dist/proot"
proot_sha256_checksum="1a2576c66f363117d8ee7ff5f1e0e4e5b217c59cc975164ffd8bbcc37155aa41"

curl -o proot $proot_url
sha256sum --check --strict <<< "${proot_sha256_checksum}  proot"

chmod +x proot

# Download Nix installer
nix_release_url="https://releases.nixos.org/nix/nix-2.3.7/nix-2.3.7-x86_64-linux.tar.xz"
nix_sha256_checksum="d77c1fd1d6bc597bce390455313e5e42b1bc3d5752994059eed69d43588d022b"
nix_release_name="nix-2.3.7-x86_64-linux"
nix_release_tarball="${nix_release_name}.tar.xz"

curl -O $nix_release_url
sha256sum --check --strict <<< "${nix_sha256_checksum}  ${nix_release_tarball}"

# Extract Nix install package
tar xf $nix_release_tarball
cd $nix_release_name

# PROOT_NO_SECCOMP=1 is needed to work on Gemini
export PROOT_NO_SECCOMP=1

# Execute installer in PRoot environment
$scratch_dir/proot -b $scratch_dir/nix-store:/nix ./install --no-daemon --no-channel-add
