#!/bin/bash

# This script will run a offline benchmark testcase using CBC and will log the output and memory/CPU statistics.
# Use with `qsub -M 'YOUR_EMAIL@students.uu.nl' run-offline-benchmark.sh BENCHMARK_NUMBER MODE`.

#$ -S /bin/bash
#$ -cwd
#$ -N run-cbc-benchmark
#$ -m bes

# Limit runtime to ten minutes
#$ -l h_rt=0:10:00

set -euxo pipefail

scratch_dir=/scratch/${USER}
export PROOT_NO_SECCOMP=1

benchmark_number=$1

$scratch_dir/proot -b ${scratch_dir}/nix-store:/nix bash <<EOF
  set -euxo pipefail
  . ~/.nix-profile/etc/profile.d/nix.sh

  cd ${scratch_dir}/python-prototype
  nix run -f default.nix package -c cli offline ./testcases/Exact${benchmark_number}.txt
EOF
